import { api } from "src/boot/axios";

export async function getProducts(){
    try {
        const products = await api.get("/products");
        return products;
    } catch (error) {
        console.log(error);
    }
    
}

export async function createProduct(product){
    try {
        const products = await api.post("/products", product);
        return products;
    } catch (error) {
        console.log(error);
    }
    
}

async function modifyProduct(id, product){
    try {
        const products = await api.put("/products/" + id, product);
        return products;
    } catch (error) {
        console.log(error);
    }
}

async function deleteProduct(id){
    try {
        const products = await api.delete("/products/" + id);
        return products;
    } catch (error) {
        console.log(error);
    }
    
}