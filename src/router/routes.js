
const routes = [
  {
    path: '/',
    component: () => import('layouts/CrudLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
    ]
  },


  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
